import React from "react";

import classes from './NavigationItems.css'

import NavigationItem from './NavigationItem/NavigationItem'

import { connect } from 'react-redux'

const NavigationItems = ({ isAuthenticated }) => {
  return (
    <ul className={classes.NavigationItems}>
      <NavigationItem link="/" exact>Burger Builder</NavigationItem>   
      {isAuthenticated ? <NavigationItem link="/orders">Orders</NavigationItem> : null}
      {isAuthenticated ? <NavigationItem link="/logout" exact>Logout</NavigationItem> : <NavigationItem link="/auth" exact>Authentification</NavigationItem>}
    </ul>
  );
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

export default connect(mapStateToProps)(NavigationItems)