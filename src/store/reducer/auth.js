import * as actionTypes from "../actions/actionTypes";

import { updateObject } from "../../shared/utility";

const initialState = {
  token: null,
  userId: null,
  error: null,
  loading: false,
  authRedirectPath: '/'
};

const authStart = state => {
  return updateObject(state, { error: null, loading: true });
};

const authError = (state, action) => {
  return updateObject(state, { error: action.error, loading: false });
};

const authSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    userId: action.authData.email,
    token: action.authData.idToken
  });
};

const authLogout = (state, action) => {
  return updateObject(state, { token: null, userId: null })
}


const setAuthRedirectPath = (state, action) => {
  return updateObject(state, { authRedirectPath: action.payload })
}


const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START:
      return authStart(state);
    case actionTypes.AUTH_FAIL:
      return authError(state, action);
    case actionTypes.AUTH_SUCCESS:
      return authSuccess(state, action);
    case actionTypes.AUTH_LOGOUT:
      return authLogout(state, action)
    case actionTypes.SET_AUTH_REDIRECT_PATH:
      return setAuthRedirectPath(state, action)
    default:
      return state;
  }
};

export default reducer;
